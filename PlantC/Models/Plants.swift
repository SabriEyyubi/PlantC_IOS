//
//  Plants.swift
//  PlantC
//
//  Created by Sabri Eyyubi on 26.04.2022.
//

import Foundation



struct Plants: Encodable, Decodable{
    var uid: String
    var name: String
    var description: String
    var imageUrl: String
    
}
