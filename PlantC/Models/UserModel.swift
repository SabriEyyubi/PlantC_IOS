//
//  UserModel.swift
//  PlantC
//
//  Created by Sabri Eyyubi on 24.04.2022.
//

import Foundation


struct User: Encodable, Decodable{
    var uid: String
    var email: String
    var usernama: String
    
}
