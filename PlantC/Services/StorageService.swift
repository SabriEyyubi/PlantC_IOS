//
//  StorageService.swift
//  PlantC
//
//  Created by Sabri Eyyubi on 24.04.2022.
//

import Foundation
import Firebase
import FirebaseAuth
import FirebaseStorage


class StrogeService {
    
    static  var storage = Storage.storage()
    static var storageRoot = storage.reference(forURL: "gs://plantc-f29ca.appspot.com/predictedPlants")
    
    static var storagePredictedPlants = storageRoot.child("predictedPlants")
    
    static func storagePlantName(plantName: String) -> StorageReference {
        return storagePredictedPlants.child(plantName)
    }
    
    static func saveImage(image: Data, metaData: StorageMetadata, storageImageRef: StorageReference, onSuccess: @escaping(_ image: Data) -> Void, onError: @escaping(_ errorMessage: String) -> Void) {
        
        storageImageRef.putData(image, metadata: metaData) {
            (StorageMetadata, error) in
            if error != nil {
                onError(error!.localizedDescription)
                return
            }
            
          
            }
        
    }
    
    
}
