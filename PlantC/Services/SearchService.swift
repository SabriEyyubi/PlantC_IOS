//
//  SearchService.swift
//  PlantC
//
//  Created by Sabri Eyyubi on 26.04.2022.
//

import Foundation
import Firebase



class SearchService {
   
    static func searchPlant(input: String, onSuccess: @escaping(_ plant: [Plants]) -> Void) {
        
        AuthService.storeRoot.collection("plants").whereField("name", arrayContains: input.lowercased().removeWhiteSpace()).getDocuments {
            (querySnapshot, err) in
            guard let snap = querySnapshot else {
                print("Error")
                return
                
            }
            
            var plants = [Plants]()
            for document in snap.documents {
                let dict = document.data()
                guard let decoded = try? Plants.init(fromDictionary: dict) else {
                    return
                }
                onSuccess(plants)
            }
            
           
        }
    }
    
   
}
