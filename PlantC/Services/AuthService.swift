//
//  AuthService.swift
//  PlantC
//
//  Created by Sabri Eyyubi on 24.04.2022.
//

import Foundation
import Firebase
import FirebaseAuth
import FirebaseStorage
import FirebaseFirestore

class AuthService {
    
    static var storeRoot = Firestore.firestore()
    
    static func getPlant(name: String) -> DocumentReference {
        return storeRoot.collection("plants").document(name)
    }
    
    static func getUserId(userId: String) -> DocumentReference {
        return storeRoot.collection("users").document(userId)
    }
    
    static func signUp(userName: String, email: String, password: String, onSuccess:@escaping(_ user: User) -> Void  ,onError: @escaping(_ errorMessage: String) -> Void){
        
        Auth.auth().createUser(withEmail: email, password: password) {
            (authData, error) in
            
            if error != nil {
                onError(error!.localizedDescription)
                return
            }
            
            guard let  userId = authData?.user.uid else {return}
            
        }
        
    }
    
    static func signIn(email: String, password: String, onSuccess:@escaping(_ user: User) -> Void  ,onError: @escaping(_ errorMessage: String) -> Void){
        Auth.auth().signIn(withEmail: email, password: password){
            (authData, error) in
            if error != nil {
                onError(error!.localizedDescription)
                return
            }
            guard let  userId = authData?.user.uid else {return}
            
            let firestoreUserId = getUserId(userId: userId)
            
            firestoreUserId.getDocument {
                (document, error) in
               if let dict = document?.data(){
                    guard let decodedUser = try? User.init(fromDictionary: dict) else {return}
                   onSuccess(decodedUser)
                }
            }
        }
    }
}
