//
//  PlantCApp.swift
//  PlantC
//
//  Created by Sabri Eyyubi on 13.03.2022.
//

import SwiftUI
import Firebase

class AppDelegate: UIResponder, UIApplicationDelegate {

  

  func application(_ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions:
                   [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
      print("Firebase is up.")
      FirebaseApp.configure()
    return true
  }
}


@main
struct PlantCApp: App {
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate
    
    var body: some Scene {
        WindowGroup {
            SignIn()
           // Main()
            //HomePage()
        }
    }
}
