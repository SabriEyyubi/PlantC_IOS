//
//  PlantData.swift
//  PlantC
//
//  Created by Sabri Eyyubi on 14.03.2022.
//

import SwiftUI

struct Plant: Identifiable{
    var id = UUID()
    var image: String
    var content: String
    var title: String
}

let plantData: [Plant] = [
    Plant(image: "myimage2", content: "LoremIpsumLore", title: "Title"),
    Plant(image: "myimage2", content: "LoremIpsumLore", title: "Title2"),
    Plant(image: "myimage2", content: "LoremIpsumLore", title: "Title3"),
    Plant(image: "myimage2", content: "LoremIpsumLore", title: "Title4"),
]
