//
//  ContentView.swift
//  PlantC
//
//  Created by Sabri Eyyubi on 7.03.2022.
//

import SwiftUI
import Firebase
import FirebaseAuth



struct SignIn: View {
    
    
    @State private var email: String = ""
    @State private var password: String = ""
    
    @State var isLoggedIn = false
    @State var showSignUp = false
    
    @State private var error: String = ""
    @State private var showingAlert = false
    @State private var alertTitle: String = "Warning"
    @State var imageNameArray = []
    
    func getImageNames(){
        self.imageNameArray = []
        let db = Firestore.firestore()
        
        
        db.collection("searchedPlantNames").order(by: "timestamp", descending: true).limit(to: 3).getDocuments(completion: {
            (documents, error) in
            guard let documents = documents else {
                       print("NO DOCUMENTS")
                       return
                   }
            var i = 0
            for documents in documents.documents{
                let name = documents.get("name") as! String
                imageNameArray.append(name)
            }
           print(imageNameArray)
            
        })
    }
    
    func errorCheck() -> String? {
        
        if email.trimmingCharacters(in: .whitespaces).isEmpty ||
            password.trimmingCharacters(in: .whitespaces).isEmpty
            {
            
            return "Please fill in all fields."
            
        }
        return nil
        
    }
    
    func login() {
        if let error = errorCheck() {
            self.error = error
            self.showingAlert = true
            
            return
        }
            Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
                if error != nil {
                    print(error?.localizedDescription ?? "")
                    print("Error \(error)")
                    self.error = error?.localizedDescription ?? ""
                    
                    self.showingAlert = true
                    return
                } else {
                    print("Login is succesfull")
                    self.isLoggedIn = true
                }
            }
        
    }
    
    
    
    
    var body: some View {
        
        NavigationView{
            
            
            VStack{
                Image("logoo")
                    .resizable()
                        .aspectRatio(contentMode: .fill)
                        .frame(width: 200.0, height: 200.0)
                        .padding(.bottom, 80)
                        .clipped()
                    Spacer()
                
              
                VStack{
                    TextField("E-mail", text: $email)
                                    .padding()
                                    .background(.white)
                                    .textInputAutocapitalization(.never)
                                    .cornerRadius(10.0)
                                    .overlay(
                                            RoundedRectangle(cornerRadius: 16)
                                                .stroke(Color.black, lineWidth: 1)
                                        )
                                    .padding(.bottom, 100)
                                    .frame(width: 360, height: 100)
                    ZStack{
                        SecureField("Password", text: $password)
                                        .padding()
                                        .textInputAutocapitalization(.never)
                                        .background(.white)
                                        .cornerRadius(10.0)
                                        .overlay(
                                                RoundedRectangle(cornerRadius: 16)
                                                    .stroke(Color.black, lineWidth: 1)
                                            )
                                        .padding(.bottom, 120)
                                        .frame(width: 360, height: 100)
                        
                        NavigationLink(destination: ResetPassword(), label: {
                            Text("Forget Password ?")
                                .fontWeight(.bold)
                                .foregroundColor(.gray)
                                .padding(.bottom, 30)
                        })
                            .frame(maxWidth: .infinity, alignment: .trailing)
                            .padding(.trailing, 20)
                        
                            
                            
                            
                           
                            
                    }
                    
                        
                        
                        
                       
                }
                    //Forget Password
                    
                    
                ZStack{
                    //Sign in button
                    
                    NavigationLink(destination: HomePage().navigationBarHidden(true), isActive: $isLoggedIn) {
                        Button(action: {
                            login()
                            print(self.error)
                           
                           
                            
                        }, label: {
                            Text("Sign In")
                                .font(.system(size: 20, weight: .bold))
                                .foregroundColor(.white)
                                .padding()
                                .frame(width: 360, height: 45, alignment: .center)
                                .background(Color.green)
                                .cornerRadius(10.0)
                            
                        })
                            
                    }
                   
                        .alert(isPresented: $showingAlert){
                            Alert(title: Text(alertTitle), message: Text(error), dismissButton: .default(Text("OK")))}
                    .padding(.bottom, 100)
                        
                    
                    //.frame(maxWidth: .infinity, alignment: .leading)
                        .padding(.top, 10)
                    VStack{
                        Text("").padding(.top, 35)
                        HStack{
                            NavigationLink(destination: HomePage().navigationBarHidden(true)){
                                Text(" Continue Without Log In")
                                    .font(.system(size: 20, weight: .bold))
                                    .foregroundColor(.white)
                                    .padding()
                                    .frame(width: 360, height: 45, alignment: .center)
                                    .background(Color.green)
                                    .cornerRadius(10.0)
                                    
                            }
                            .navigationBarBackButtonHidden(true)
                           
                            /*
                             .sheet(isPresented: $showSignUp, content: {
                                 SignUp()
                             })*/
                        }
                        
                    }
                    .navigationBarBackButtonHidden(true)
                    
                }
                
                .padding()
                .padding(.bottom, 35)
                .frame(maxHeight: .infinity)
                
                .overlay(
                    VStack{
                       
                        HStack{
                            Text(" Don't Have Account?")
                                .fontWeight(.bold)
                                .foregroundColor(.gray)
                            
                            NavigationLink(destination: SignUp()){
                                Text("Sign Up").foregroundColor(Color.green)
                                
                            }
                                
                            
                        }
                    }
                        .padding(.bottom, 25)
                    
                    
                    ,alignment: .bottom
                    
                )
                
                
                
            }
            .onAppear(perform: {
               // getImage()//
                getImageNames()
            })
        }.navigationBarTitle("")
            .navigationBarHidden(true)
            .navigationBarBackButtonHidden(true)
            
        
        
        
        
    }
    
    
    func clear(){
        self.email = ""
        self.password = ""
    }
    
    
    
    }


struct ContentView_Previews: PreviewProvider {
    
    static var previews: some View {
        Group {
            SignIn()
           // SignIn()
        }
    }
}

