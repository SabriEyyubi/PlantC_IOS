//
//  Profile.swift
//  PlantC
//
//  Created by Sabri Eyyubi on 13.03.2022.
//

import SwiftUI
import Firebase
import FirebaseAuth

struct Profile: View {
    @State var username = ""
    @State var email = ""
    @State var isLogout: Bool = false
    
    func logOut(){
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
            self.isLogout = true
            print("Success")
        } catch let signOutError as NSError {
            print("Error signing out \(signOutError)")
        }
    }
    
    func getDetail(){
        if Auth.auth().currentUser != nil {
            let user = Auth.auth().currentUser
            if let user = user {
              // The user's ID, unique to the Firebase project.
              // Do NOT use this value to authenticate with your backend server,
              // if you have one. Use getTokenWithCompletion:completion: instead.
              let uid = user.uid
                self.email = user.email!
              print(email, uid)
              }
              // ...
            }
         else {
          print("User is not exist")
        }
    }
    
    
    var body: some View {
        VStack{
            Image("logo")
                .resizable()
                    //.aspectRatio(contentMode: .fill)
                    .frame(width: 200.0, height: 200.0)
                    .clipShape(RoundedRectangle(cornerRadius: 16, style: .continuous))
                    .shadow(color: Color.black.opacity(0.3), radius: 6, x: 0, y: 3)
                    .padding(.top, 50)
                        
              
            Text("Account Information")
                .padding()
           
            VStack{
                Text("Username")
                    .font(.title3)
                    .padding(.bottom, 20)
                Text("Username")
            }
           
            .padding(.bottom, 40)
                
            
            VStack{
                Text("Email")
                    .font(.title3)
                    .padding(.bottom, 30)
                Text(email)
            }
            
            .padding(.bottom, 40)
            
           
           
               
                    NavigationLink(destination: EditProfile().navigationBarHidden(false), label: {
                        Text("Edit Profile")
                            .font(.system(size: 20, weight: .bold))
                            .foregroundColor(.white)
                            .padding()
                            .frame(width: 360, height: 45, alignment: .center)
                            .background(Color.green)
                            .cornerRadius(10.0)
                    })
                //.frame(maxWidth: .infinity, alignment: .leading)
                    .padding(.top, 10)
                    .padding(.bottom, 20)
                VStack{
                    HStack{
                        NavigationLink(destination: SignIn(), isActive: $isLogout) {
                            Button(action: {
                               logOut()
                                print("User Log out")
                                
                               
                                
                            }, label: {
                                Text("Log Out")
                                    .font(.system(size: 20, weight: .bold))
                                    .foregroundColor(.white)
                                    .padding()
                                    .frame(width: 360, height: 45, alignment: .center)
                                    .background(Color.green)
                                    .cornerRadius(10.0)
                                
                            })
                        }
                        }
                        
                       
                        
                    }
                    
                }
        .onAppear(perform: {
            getDetail()
        })
            .padding(.bottom, 90)
                
            
           
        }
        
        
    }


struct Profile_Previews: PreviewProvider {
    static var previews: some View {
        Profile()
    }
}
