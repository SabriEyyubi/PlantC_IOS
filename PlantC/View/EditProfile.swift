//
//  EditProfile.swift
//  PlantC
//
//  Created by Sabri Eyyubi on 16.04.2022.
//

import SwiftUI

struct EditProfile: View {
    @State var username = ""
    @State var password = ""
    @State var verifyPassword = ""
    
    var body: some View {
        VStack{
            Image("logo")
                .resizable()
                    //.aspectRatio(contentMode: .fill)
                .frame(width: 150, height: 150, alignment: .center)
                    .clipShape(RoundedRectangle(cornerRadius: 16, style: .continuous))
                    .shadow(color: Color.black.opacity(0.3), radius: 6, x: 0, y: 3)
                    .padding(.top, 50)
            
                        
              
            Text("Edit Profile Details")
                .padding()
            
            VStack(alignment: .leading, spacing: 8){
                Text("Change your user name")
                    .fontWeight(.bold)
                    .foregroundColor(.black)
                
                
                TextField("Username", text: $username)
                                .padding()
                                .background(.white)
                                .cornerRadius(10.0)
                                .overlay(
                                        RoundedRectangle(cornerRadius: 16)
                                            .stroke(Color.black, lineWidth: 1)
                                    )
                                .frame(width: 360, height: 100)
                
                
                Text("Change your password")
                    .fontWeight(.bold)
                    .foregroundColor(.black)
                
                
                TextField("Password", text: $password)
                                .padding()
                                .background(.white)
                                .cornerRadius(10.0)
                                .overlay(
                                        RoundedRectangle(cornerRadius: 16)
                                            .stroke(Color.black, lineWidth: 1)
                                    )
                                .frame(width: 360, height: 100)
                
                
                Text("Verify Password")
                    .fontWeight(.bold)
                    .foregroundColor(.black)
                
                
                TextField("Password", text: $verifyPassword)
                                .padding()
                                .background(.white)
                                .cornerRadius(10.0)
                                .overlay(
                                        RoundedRectangle(cornerRadius: 16)
                                            .stroke(Color.black, lineWidth: 1)
                                    )
                                .frame(width: 360, height: 100)
                
                
                
            }
            
            NavigationLink(destination: Profile().navigationBarHidden(false), label: {
                Text("Save")
                    .font(.system(size: 20, weight: .bold))
                    .foregroundColor(.white)
                    .padding()
                    .frame(width: 100, height: 55, alignment: .center)
                    .background(Color.green)
                    .cornerRadius(10.0)
            })
            
        }
        .padding(.bottom, 110)
    }
}

struct EditProfile_Previews: PreviewProvider {
    static var previews: some View {
        EditProfile()
    }
}
