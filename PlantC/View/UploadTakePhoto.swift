//
//  UploadTakePhoto.swift
//  PlantC
//
//  Created by Sabri Eyyubi on 13.03.2022.
//

import SwiftUI
import Alamofire

struct UploadTakePhoto: View {
    
    @State var showActionSheet = false
    @State var showImagePicker = false
    @State var sourceType : UIImagePickerController.SourceType = .camera
    @State var prediction = ""
    @State var URL = "https://wikipedia.org/w/api.php"
    @State var stringDeneme: [String] = []
    
    @State var description = ""
    @State var probability = ""
    @State var onlyPrediction = ""
    @State var number = 0.0

    
    @State var image: UIImage?
    var body: some View {
        
       
        VStack{
            
                
                if image != nil{
                    Image(uiImage: image!)
                        .resizable()
                        .scaledToFit()
                        .frame(width: 350, height: 350, alignment: .top)
                        .padding()
                        .padding(.top, 50)
                    
                } else{
                    Image("logoo")
                        .padding(.top, 75)
                }
                Spacer()
                Button(action: {
                    self.showActionSheet = true
                }){
                    /*
                     if image == nil{
                         Text("Upload or Take An Image")
                             .font(.system(size: 20, weight: .light, design: .default))
                             .foregroundColor(Color("ColorGreenMedium"))
                             //.font(.system(size: 20, weight: .light, design: .default))
                             .cornerRadius(15)
                             .foregroundColor(.white)
                             .frame(width: 250, height: 50, alignment: .center)
                             .background(Color.green)
                             .cornerRadius(10.0)
                     }
                     else{
                         Text(" Upload Another Image")
                             .font(.system(size: 20, weight: .light, design: .default))
                             .foregroundColor(Color("ColorGreenMedium"))
                            // .font(.system(size: 20, weight: .light, design: .default))
                             .cornerRadius(15)
                             .foregroundColor(.white)
                             .frame(width: 250, height: 50, alignment: .center)
                             .background(Color.green)
                             .cornerRadius(10.0)
                     }
                     */
                   
                        Image(uiImage: UIImage(named: "uploadTakePhoto")!)
                        .resizable()
                        .renderingMode(.original)
                        .frame(width: 235, height: 125, alignment: .center)
                       // .background(Color(.green))
                        //.renderingMode(.original)
                  
                    
                    
                }
                .padding()
                .padding(.bottom, 50)
                .actionSheet(isPresented: $showActionSheet){
                    ActionSheet(title: Text("Add a picture"), message: nil, buttons:[
                        //Button 1
                        .default(Text("Camera"), action: {
                            self.showImagePicker = true
                            self.sourceType = .camera
                        }),
                        .default(Text("Photo Library"), action: {
                            self.showImagePicker = true
                            self.sourceType = .photoLibrary
                        }),
                        .cancel()
                        
                    ])
                }.sheet(isPresented: $showImagePicker){
                    imagePicker(image: self.$image, showImagePicker: self.$showImagePicker, sourceType: self.sourceType)
                }
            
            NavigationLink(destination: Detail(prediction: $onlyPrediction, uploadedImage: $image, number: $number, description: $description).onAppear {
                self.upload()
                let seconds = 2.0
                DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
                self.requestInfo(flowerName: onlyPrediction)
                }
            }){
                /**/
                Text("Continue")
                    .font(.system(size: 20, weight: .light, design: .default))
                    .cornerRadius(15)
                    .foregroundColor(.white)
                    .frame(width: 250, height: 50, alignment: .center)
                    .background(Color.green)
                    .cornerRadius(10.0)
                    .padding(.bottom, 80)
                
                 
            }
            .navigationBarTitle("Back")
            
            

            
            
    }
}
    
    func upload()
        {
            let uiImage : UIImage = self.image ?? UIImage()
            let imageData : Data = uiImage.jpegData(compressionQuality: 0.1) ?? Data()
            
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                        multipartFormData.append(imageData, withName: "file", fileName: "file", mimeType: "image/jpg")
                    }, to: "http://127.0.0.1:5000/predict") { (result) in
                        switch result {
                        case .success(let upload, _, _):
                            upload.uploadProgress(closure: { (progress) in
                                print("Upload Progress: \(progress.fractionCompleted)")
                            })

                            upload.responseJSON { response in
                                print("Success")
                                
                                
                                if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                                        print("Data: \(utf8Text)")
                                    self.prediction = utf8Text
                                    print("This is prediction \(self.prediction )")
                                    var splits =  prediction.components(separatedBy: ",")
                                    self.onlyPrediction = splits[0]
                                    self.probability = splits[1]
                                    print(self.onlyPrediction)
                                    print("--------")
                                    
                                    let formatter = NumberFormatter()
                                    formatter.locale = Locale.current
                                    self.number = formatter.number(from: probability) as! Double
                                    print(number)
                                    }
                            }

                        case .failure(let encodingError):
                            print("Error")
                            print(encodingError)
                        }
                    }
                }
    func requestInfo(flowerName: String){
        let parameters : [String:String] = [
          "format" : "json",
          "action" : "query",
          "prop" : "extracts|pageimages",
          "exintro" : "",
          "explaintext" : "",
          "titles" : flowerName,
          "indexpageids" : "",
          "redirects" : "1",
          "pithumbsize": "500"
          ]
        Alamofire.request(URL, method: .get, parameters: parameters).responseJSON { (response) in
           
                DispatchQueue.main.async {
                 // print(response.result)
                   // let flowerJSON = response.data
                    //print(flowerJSON)
                    if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                           print("Data: \(utf8Text)")
                       
                      
                        let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
                        print("--------------------------- \(String(describing: json))")
                        let pageId = json?["query"]?["pageids"]
                        self.stringDeneme = pageId as? [String] ?? [""]
                        print(pageId as Any)
                       let pages = json?["query"]?["pages"]
                    print(type(of: pages))
                        //let name = (responseObject as! [String : AnyObject])["name"]
                        self.description = (pages as? [String: AnyObject])?[stringDeneme[0]]?["extract"] as! String
                        print("Description \(self.description)")

                       
                       
                       // let description = pages?[pageId]
                        
                        
                        
                        }
                  //  let pageId = flowerJSON?["query"]["pageids"][0]
                   
                    
                                    }
                    /*
                    
                     let flowerDescription = flowerJSON["query"]["pages"][pageId]["extract"].stringValue
                     let flowerImage = flowerJSON["query"]["pages"][pageId]["thumbnail"]["source"].stringValue
                     self.descriptionLabel.text = flowerDescription
                     
                     self.imageView.sd_setImage(with: Foundation.URL(string: flowerImage))
                     print(flowerDescription)*/
                   
                }
            
        }
    
    func requestInfoWithRedirect(flowerName: String){
        
        let parameters : [String:String] = [
          "format" : "json",
          "action" : "query",
          "prop" : "extracts|pageimages",
          "exintro" : "",
          "explaintext" : "",
          "titles" : flowerName,
          "indexpageids" : "",
         // "redirects" : "1",
          "pithumbsize": "500"
          ]
        @State var redirectURL = "https://en.wikipedia.org/w/api.php?action=query&list=backlinks&blfilterredir=redirects&bltitle=\(flowerName)&bllimit=max"
        
        DispatchQueue.main.async {
         // print(response.result)
           // let flowerJSON = response.data
            //print(flowerJSON)
            Alamofire.request(URL, method: .get, parameters: parameters).responseJSON { (response) in
               
                    DispatchQueue.main.async {
                     // print(response.result)
                       // let flowerJSON = response.data
                        //print(flowerJSON)
                        if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                               print("Data: \(utf8Text)")
                        }
                    }
            }
        
        
    }
    }
        
        
    }
    


struct UploadTakePhoto_Previews: PreviewProvider {
    static var previews: some View {
        UploadTakePhoto()
    }
}
