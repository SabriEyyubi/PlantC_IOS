//
//  Search.swift
//  PlantC
//
//  Created by Sabri Eyyubi on 13.03.2022.
//

import SwiftUI
import Firebase
import FirebaseStorage

struct Search: View {
    @ObservedObject var data = getData()
    
    var body: some View {
       
        
        ZStack(alignment: .top){
            
            GeometryReader{_ in
                
               
                
            }.background(Color("Color").edgesIgnoringSafeArea(.all))
            
            CustomSearchBar(data: self.$data.datas).padding(.top)
            
        }.navigationBarTitle("Search")
        //.navigationBarHidden(true)

        
    }
}



struct CustomSearchBar : View {
    
    @State var txt = ""
    @Binding var data : [dataType]
    
    var body : some View{
        
        VStack(spacing: 0){
            
            HStack{
                
                TextField("Search", text: self.$txt)
                
                if self.txt != ""{
                    
                    Button(action: {
                        
                        self.txt = ""
                        
                    }) {
                        
                        Text("Cancel")
                    }
                    .foregroundColor(.black)
                    
                }

            }.padding()
                .cornerRadius(15)
            
           
            if self.txt != ""{
                
                if  self.data.filter({$0.name.lowercased().contains(self.txt.lowercased())}).count == 0{
                    
                    Text("No Results Found").foregroundColor(Color.black.opacity(0.5)).padding()
                }
                else{
                    
                List(self.data.filter{$0.name.lowercased().contains(self.txt.lowercased())}){i in
                            
                    NavigationLink(destination: Details(data: i)) {
                        
                        Text(i.name)
                    }
                            
                        
                    }.frame(height: UIScreen.main.bounds.height / 5)
                }

            }
            
            
        }.background(Color.white)
        .padding()
        
    }
}

class getData : ObservableObject{
    
    @Published var datas = [dataType]()
    
    init() {
        
        let db = Firestore.firestore()
        
        db.collection("items").getDocuments { (snap, err) in
            
            if err != nil{
                
                print((err?.localizedDescription)!)
                return
            }
            
            for i in snap!.documents{
                
                let id = i.documentID
                let name = i.get("name") as! String
                let url = i.get("url") as! String
                let description = i.get("description") as! String
                
                self.datas.append(dataType(id: id, name: name, url:url, description: description))
            }
        }
    }
}

struct dataType : Identifiable {
    
    var id : String
    var name : String
    var url: String
    var description: String
}



struct Details : View {
    
    var data : dataType
    let FILE_NAME = "predictedPlants/rose.jpeg"
    @State var imageURL = ""
    @State var image: UIImage = UIImage()
    @State var imageArray = [UIImage]()
    @State var imageNameArray = []
    
    
   
    
    func getImage(){
        let storageRef = Storage.storage().reference().child("searchImages/\(data.name).jpeg")
        
        storageRef.getData(maxSize: 6 * 1024 * 1024) { data, error in
           if let error = error {
             // Uh-oh, an error occurred!
               print(error.localizedDescription)
           } else {
               print("Image come")
             // Data for "images/island.jpg" is returned
               self.image = UIImage(data: data!) ?? UIImage()
           }
         }
    }
    
    func uploadSearched(){
        let uploadName = "searchedPlants/\(data.name).jpeg"
        let storage = Storage.storage().reference()
        
        let metadata = StorageMetadata()
        metadata.contentType = "image/jpeg"
        
        storage.child(uploadName).putData(image.jpegData(compressionQuality: 0.42)!, metadata: metadata){
            (metadata, error) in
            guard let metadata = metadata else{
                print(error?.localizedDescription)
                return
            }
            
            let size = metadata.size
            print("Upload size is \(size)")
            print("Upload Success")
            downloadSearchPredictedImages()
            let db = Firestore.firestore()
            db.collection("searchedPlantNames").document(data.name).setData([ "name": data.name, "timestamp": FieldValue.serverTimestamp() ], merge: true)
            
            db.collection("searchedPlantNames").order(by: "timestamp", descending: true).limit(to: 3).getDocuments(completion: {
                (documents, error) in
                guard let documents = documents else {
                           print("NO DOCUMENTS")
                           return
                       }
                
                for documents in documents.documents{
                    let name = documents.get("name") as! String
                    imageNameArray.append(name)
                }
                print(imageNameArray)
                
            })
          
            
        }
    }
    
    func downloadSearchPredictedImages(){
        // For last predicted and searched plants
        let storage = Storage.storage().reference()
        let storageReference = storage.child("searchedPlants/")
        storageReference.list(maxResults: 3) { (result, error) in
          if let error = error {
            // ...
          }
           // print(\())
          for prefix in result.prefixes {
            // The prefixes under storageReference.
            // You may call listAll(completion:) recursively on them.
              print(prefix)
          }
            var i = 0
          for item in result.items {
            // The items under storageReference.
              print(item.name)
             // imageNameArray.append(item.name)
              print(imageNameArray)
              var name = item.name
              downloadImage(image: String(name))
              print("---------\(imageNameArray[0])")
             // print(imageArray)
          }
            
        }
        
        
    }
    
    func downloadImage(image: String){
        var downloadedImage: UIImage = UIImage()
        let storageRef = Storage.storage().reference().child("searchedPlants/\(image)")
        
        storageRef.getData(maxSize: 6 * 1024 * 1024) { data, error in
           if let error = error {
             // Uh-oh, an error occurred!
               print(error.localizedDescription)
           } else {
               print("Image downloading")
             // Data for "images/island.jpg" is returned
               downloadedImage = UIImage(data: data!) ?? UIImage()
               self.imageArray.append(downloadedImage)
               self.image = downloadedImage
           }
         }
    }
    
    
    
    var body : some View{
        
       
        //Text(data.url)
        
        
        VStack{
            Image(uiImage: self.image)
                .resizable()
                    //.aspectRatio(contentMode: .fill)
                .frame(width: 325, height: 325, alignment: .center)
                    .shadow(color: Color.black.opacity(0.3), radius: 6, x: 0, y: 3)
                    //.padding(.top, 50)
                    .padding()
            Text(data.name)
                .padding()
            TextEditor(text: .constant(data.description))
                .padding()
            Spacer()
        }.onAppear(perform: {
            getImage()
            let seconds = 4.0
            DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
                // Put your code which should be executed with a delay here
                uploadSearched()
            }
            
        })
        
       
        

    }
}



