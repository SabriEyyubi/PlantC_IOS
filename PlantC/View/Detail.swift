//
//  Detail.swift
//  PlantC
//
//  Created by Sabri Eyyubi on 19.03.2022.
//

import SwiftUI
import Firebase
//import Alamofire



struct Detail: View {
    //let param: [String:Any] = ["your_parameters": value]
    @Binding var prediction: String
    @Binding var uploadedImage: UIImage?
    //@State var probability = ""
    //@Binding var onlyrediction = ""
    @Binding var number: Double
    @Binding var description: String
    @State var imageNameArray = []
    @State var showingAlert = false
    
    func uploadPredicted(){
        let uploadName = "predictedPlants/\(prediction).jpeg"
        let storage = Storage.storage().reference()
        
        let metadata = StorageMetadata()
        metadata.contentType = "image/jpeg"
        
        storage.child(uploadName).putData((uploadedImage?.jpegData(compressionQuality: 0.42)!)!, metadata: metadata){
            (metadata, error) in
            guard let metadata = metadata else{
                print(error?.localizedDescription)
                return
            }
            
            let size = metadata.size
            print("Upload size is \(size)")
            print("Upload Success")
            let db = Firestore.firestore()
            db.collection("predictedPlantNames").document(prediction).setData([ "name": prediction,"probability": (String(format: "%.2f", self.number)) ,"timestamp": FieldValue.serverTimestamp() ], merge: true)
            
            db.collection("predictedPlantNames").order(by: "timestamp", descending: true).limit(to: 3).getDocuments(completion: {
                (documents, error) in
                guard let documents = documents else {
                           print("NO DOCUMENTS")
                           return
                       }
                
                for documents in documents.documents{
                    let name = documents.get("name") as! String
                    imageNameArray.append(name)
                }
                print(imageNameArray)
                
            })
          
            
        }
    }
    
    var body: some View {
        //Text("Detail")
        VStack(){
        //    Image(systemName: "logo")
            Image(uiImage: uploadedImage!)
                .resizable()
                    //.aspectRatio(contentMode: .fill)
                .frame(width: 325, height: 325, alignment: .center)
                    .shadow(color: Color.black.opacity(0.3), radius: 6, x: 0, y: 3)
                    //.padding(.top, 50)
                   // .padding()
                    .padding(.bottom, 30)
            
            Text(" Prediction: \(self.prediction)")
                .font(.headline)
                .font(.system(size: 12))
                .padding()
            Text("Probability: %\(String(format: "%.2f", self.number))")
                .padding()
                .font(.system(size: 15, weight: .bold, design: .default))
            TextEditor(text: .constant(self.description))
                .padding()
                .lineSpacing(7)
            Spacer()
        }
        .padding()
        .padding(.bottom, 30)
        .alert(isPresented: $showingAlert){
           
                Alert(title: Text("Warning"), message: Text("The result may not accurate because probabilility of prediction is too low."), dismissButton: .default(Text("OK")))}
        
            
            
        .onAppear(perform: {
            let seconds = 2.0
            DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
                // Put your code which should be executed with a delay here
                uploadPredicted()
                showAlert()
                
            }
        })
        
       
        
        

    }
    func showAlert(){
        if self.number < 50 {
            self.showingAlert = true
                
        }
    }
    
   
    
}

struct Detail_Previews: PreviewProvider {
    
    @State static var prediction: String = ""
    @State static var uploadedImage: UIImage? = UIImage()
    @State static var number: Double = 0.0
    @State static var description: String = ""
    static var previews: some View {
        Detail(prediction: $prediction, uploadedImage: $uploadedImage, number: $number, description: $description)
    }
}
