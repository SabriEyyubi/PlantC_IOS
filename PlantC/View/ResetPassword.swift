//
//  ResetPassword.swift
//  PlantC
//
//  Created by Sabri Eyyubi on 8.05.2022.
//

import SwiftUI
import Firebase

struct ResetPassword: View {
    @Environment(\.presentationMode) var presentation
    @State var email: String = ""
    @State var error:String = ""
    @State var showAlertView: Bool = false
    var body: some View {
        NavigationView{
            ZStack{
                //background color
                Color.white.ignoresSafeArea(edges: .all)
                
                VStack(spacing: 20){
                    TextField("E-mail", text: $email)
                                    .padding()
                                    .font(.system(size: 18))
                                    .background(.white)
                                    .textInputAutocapitalization(.never)
                                    .cornerRadius(10.0)
                                    .overlay(
                                            RoundedRectangle(cornerRadius: 16)
                                                .stroke(Color.black, lineWidth: 1)
                                        )
                                    .padding(.bottom, 100)
                                    .frame(width: 360, height: 100)
                    
                    Button(action: {
                        resetPassword()
                       
                    }, label: {
                        Text("Reset Password")
                            .font(.system(size: 20, weight: .bold))
                            .foregroundColor(.white)
                            .padding()
                            .frame(width: 350, height: 60, alignment: .center)
                            .background(Color.red)
                            .cornerRadius(10.0)
                            .padding(.bottom, 50)
                        
                    })
                        .alert(isPresented: $showAlertView){
                            Alert(title: Text("Warning"), message: Text(error), dismissButton: .default(Text("OK")))}
                    Spacer()
                    
                    Button(action: {
                        presentation.wrappedValue.dismiss()
                        
                    }, label: {
                        Text("Back to Login")
                            .frame(maxWidth: .infinity)
                            .font(.title3)
                            .foregroundColor(.gray)
                    })
                        .padding()
                }
            }
        }
        .navigationTitle("Reset Password").font(.system(size: 12))
        .navigationBarTitle("Back")
        
    }
    
    func resetPassword(){
        Auth.auth().sendPasswordReset(withEmail: email, completion: {
            error in
            if let err = error {
                print(err.localizedDescription)
                self.error = err.localizedDescription
                self.showAlertView = true
                return
            }
            presentation.wrappedValue.dismiss()
        })
    }
}

struct ResetPassword_Previews: PreviewProvider {
    static var previews: some View {
        ResetPassword()
    }
}
