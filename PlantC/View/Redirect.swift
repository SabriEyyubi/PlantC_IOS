//
//  Redirect.swift
//  PlantC
//
//  Created by Sabri Eyyubi on 24.04.2022.
//

import SwiftUI

struct Redirect: View {
    @EnvironmentObject var session: SessionStore
    
    func listen(){
        session.listen()
    }
    var body: some View {
        Group {
            if(session.session != nil){
                HomePage()
            }
            else{
                SignIn()
            }

        }.onAppear(perform: listen)
    }
}

struct Redirect_Previews: PreviewProvider {
    static var previews: some View {
        Redirect()
    }
}
