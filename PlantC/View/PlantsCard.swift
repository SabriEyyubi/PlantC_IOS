//
//  PlantsCard.swift
//  PlantC
//
//  Created by Sabri Eyyubi on 14.03.2022.
//

import SwiftUI

struct PlantsCard: View {
    @Binding var name : String
    @Binding var image : UIImage
    
   
    var body: some View {
        
        ZStack{
            
            Text(name)
                .padding(.leading, 90)
                .padding(.trailing, 10)
                .padding(.vertical, 3)
                .frame(width: 250, height: 120, alignment: .center)
                .background(LinearGradient(gradient: Gradient(colors: [Color("Gradient2"), Color("Gradient22")]), startPoint: .leading, endPoint: .trailing))
                .cornerRadius(12)
            .lineLimit(6)
                .font(.footnote)
                .multilineTextAlignment(.leading)
                .foregroundColor(Color.white)
            
            Image(uiImage: image)
                .resizable()
                .frame(width: 66, height: 66, alignment: .center)
                .background(
            Rectangle()
                .fill(Color.gray)
                .frame(width: 74, height: 74, alignment: .center))
                .cornerRadius(16.0)
                .offset(x: -80)
            
          /*
           
           Text("title")
                 .font(.system(size: 20))
                 .foregroundColor(.black)
                 .frame(width: 100, height: 20, alignment: .top)
                 .lineLimit(1)
                 .padding(.bottom, 115)*/  
            
            
        }
    }
    
}

struct PlantsCard_Previews: PreviewProvider {
    @State static var name: String = ""
    @State static var image: UIImage = UIImage()
    static var previews: some View {
        PlantsCard(name: $name, image: $image)
            .previewLayout(.fixed(width: 400, height: 200))
    }
}
