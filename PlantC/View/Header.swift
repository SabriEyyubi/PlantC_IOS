//
//  Header.swift
//  PlantC
//
//  Created by Sabri Eyyubi on 14.03.2022.
//

import SwiftUI

struct Header: View {
    // MARK: - PROPERTIES
    
    @State private var showHeadline: Bool = false
    
    var slideInAnimation: Animation {
        Animation.spring(response: 1.5, dampingFraction: 0.5, blendDuration: 0.5)
            .speed(1)
            .delay(0.25)
    }
    
    var body: some View {
        
        ZStack{
            Image("DarkBack")
                .resizable()
                .aspectRatio(contentMode: .fill)
            
            HStack (alignment: .top, spacing: 0){
                Rectangle()
                    .fill(Color.green)
                    .frame(width: 4)
                VStack(alignment: .leading , spacing: 6) {
                Text("PlantC")
                    .font(.system(.title, design: .serif))
                    .fontWeight(.bold)
                    .foregroundColor(Color.white)
                    .shadow(radius: 3)
                Text("Anything you want to know about plants")
                    .font(.footnote)
                    .lineLimit(2)
                    .multilineTextAlignment(.leading)
                    . foregroundColor(Color.white)
                    .shadow(radius: 3)
            }
            .padding(.vertical, 0)
            .padding(.horizontal, 20)
            .frame(width: 281, height: 105)
            .background(Color("ColorBlackTransparentLight"))
        }
            .frame(width: 285, height: 105, alignment: .center)
            .offset(x: -33, y: showHeadline ? 75 : 220)
            .animation(slideInAnimation)
            .onAppear(perform: {
                self.showHeadline.toggle()
            })
        }
        .frame(width: 400, height: 280, alignment: .center)
    }
}

struct Header_Previews: PreviewProvider {
    static var previews: some View {
        Header()
            .previewLayout(.sizeThatFits)
    }
}
