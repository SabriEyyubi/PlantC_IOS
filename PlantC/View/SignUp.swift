//
//  SignUp.swift
//  PlantC
//
//  Created by Sabri Eyyubi on 7.03.2022.
//

import SwiftUI
import Firebase
import FirebaseAuth

struct SignUp: View {
    
    @Environment(\.presentationMode) var presentation
    @State var email: String = ""
    @State var password: String = ""
    @State var confirmPassword: String = ""
    @State var username: String = ""
    @State private var error: String = ""
    @State private var showingAlert = false
    @State private var alertTitle: String = "Warning"
    
    var body: some View {
        VStack{
            
            Image("logoo")
                .resizable()
                //.scaledToFit()
                .aspectRatio(1,  contentMode: .fit)
                    .frame(width: 150.0, height: 150.0, alignment: .center)
                    
                    
                Spacer()
            
            Text("Sign Up")
                .font(.title)
                .fontWeight(.bold)
                .kerning(1.9)
                .frame(maxWidth: .infinity, alignment: .leading)
            
            VStack(alignment: .leading, spacing: 8 , content: {
                Text("Email Adress")
                    .fontWeight(.bold)
                    .foregroundColor(.gray)
                
                TextField("E-mail", text: $email)
                                .padding()
                                .textInputAutocapitalization(.never)
                                .background(.white)
                                .cornerRadius(10.0)
                                .overlay(
                                        RoundedRectangle(cornerRadius: 16)
                                            .stroke(Color.black, lineWidth: 1)
                                    )
                               
                               
                    .padding(.top, 5)
                
                Text("Username")
                    .fontWeight(.bold)
                    .foregroundColor(.gray)
                
                TextField("Username", text: $username)
                                .padding()
                                .textInputAutocapitalization(.never)
                                .background(.white)
                                .cornerRadius(10.0)
                                .overlay(
                                        RoundedRectangle(cornerRadius: 16)
                                            .stroke(Color.black, lineWidth: 1)
                                    )
                    .padding(.top, 5)
                
                Text("Password")
                    .fontWeight(.bold)
                    .foregroundColor(.gray)
                
                SecureField("Password", text: $password)
                                .padding()
                                .textInputAutocapitalization(.never)
                                .background(.white)
                                .cornerRadius(10.0)
                                .overlay(
                                        RoundedRectangle(cornerRadius: 16)
                                            .stroke(Color.black, lineWidth: 1)
                                    )
                    .padding(.top, 5)
                
            })
                .padding(.top,25)
            
            VStack(alignment: .leading, spacing: 8 , content: {
                Text(" Confirm Password")
                    .fontWeight(.bold)
                    .foregroundColor(.gray)
                
                SecureField("Confirm Password", text: $confirmPassword)
                    .font(.system(size: 15, weight: .semibold))
                    .padding()
                    .textInputAutocapitalization(.never)
                    .background(.white)
                    .cornerRadius(10.0)
                    .overlay(
                            RoundedRectangle(cornerRadius: 16)
                                .stroke(Color.black, lineWidth: 1)
                        )
                //.foregroundColor(Color("dark"))
                    .padding(.top, 5)
                    .onChange(of: self.password) { value in
                        if value.count > 10 {
                                self.password = String(value.prefix(15))
                    }
                }
                
            })
                .padding(.top,5)
            
        
            
            
            ZStack{
                
                Button(action: {
                    createNewUser()
                    if error == "" {
                        presentation.wrappedValue.dismiss()
                    }
                   
                }, label: {
                    Text("SIGN UP")
                        .font(.system(size: 20, weight: .bold))
                        .foregroundColor(.white)
                        .padding()
                        .frame(width: 250, height: 45, alignment: .center)
                        .background(Color.green)
                        .cornerRadius(10.0)
                    
                }).alert(isPresented: $showingAlert){
                    Alert(title: Text(alertTitle), message: Text(error), dismissButton: .default(Text("OK")))
                }
                //.frame(maxWidth: .infinity, alignment: .leading)
                   
                
                    Text("ALREADY MEMBER?")
                        .padding(.top, 90)
                
                
            }
            
            
            
            
        }
        .padding()
        .frame(maxHeight: .infinity)
        
    }
    
    func errorCheck() -> String? {
        
        if email.trimmingCharacters(in: .whitespaces).isEmpty ||
            password.trimmingCharacters(in: .whitespaces).isEmpty ||
            confirmPassword.trimmingCharacters(in: .whitespaces).isEmpty ||
            username.trimmingCharacters(in: .whitespaces).isEmpty{
            
            return "Please fill in all fields."
            
        }
        return nil
        
    }
    
    func signUp() {
        if let error = errorCheck() {
            self.error = error
            self.showingAlert = true
            return
        }
        
        AuthService.signUp(userName: username, email: email, password: password, onSuccess: {
            (user) in
            clear()
        })  {
            (errorMessage) in
            print("Error \(errorMessage)")
            self.error = errorMessage
            self.showingAlert = true
            return
        }
        
        
    }
    
    private func createNewUser(){
        if let error = errorCheck() {
            self.error = error
            self.showingAlert = true
            return
        }
    
        
        Auth.auth().createUser(withEmail: email, password: password) {
            authResult, error in
            
            if let err = error {
                print("Error \(err.localizedDescription)")
                self.error = err.localizedDescription
                self.showingAlert = true
                return

            }
            
            print("Successfull")
            presentation.wrappedValue.dismiss()
        }
    }
    
    func clear(){
        self.email = ""
        self.username = ""
        self.password = ""
        self.confirmPassword = ""
    }
}

struct SignUp_Previews: PreviewProvider {
    static var previews: some View {
        SignUp()
    }
}
