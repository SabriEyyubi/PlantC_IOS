//
//  Main.swift
//  PlantC
//
//  Created by Sabri Eyyubi on 13.03.2022.
//

import SwiftUI
import Firebase



struct Main: View {
  
    @State var emptyDictionary = [String: UIImage]()
    @State var isNavigationBarHidden: Bool = true
    var plants : [Plant] = plantData
    @State var stringArray  = ["oak", "rose", "bamboo"]
    @State var predictedDefaultArray = ["Rose", "Thorn", "Viola"]
    @State var actualpredictedArray = []
    @State var defaultProbArray = ["0", "0", "0"]
    @State var  probabilityArray = []
    @State var predictedImage = UIImage()
    @State var predictedImage2 = UIImage()
    @State var predictedImage3 = UIImage()
    @State var image = UIImage()
    @State var image2 = UIImage()
    @State var image3 = UIImage()
    @State var imageNameArray = []
    @State var imageArray: [UIImage] = []
    @State var imageName = ""
    
    func downloadPredictedImage(image: [String]){
        var downloadedImage: UIImage = UIImage()
        self.imageArray = []
        var i = 0
        for item in image{
            
        
        let storageRef = Storage.storage().reference().child("predictedPlants/\(item).jpeg")
        
        storageRef.getData(maxSize: 6 * 1024 * 1024) { data, error in
           if let error = error {
             // Uh-oh, an error occurred!
               print(error.localizedDescription)
           } else {
               print("Image downloading")
               print(item)
               DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                   downloadedImage = UIImage(data: data!) ?? UIImage()
                   self.imageArray.append(downloadedImage)
                   if i == 0 {
                       self.predictedImage = downloadedImage
                   }
                   if i == 1 {
                       self.predictedImage2 = downloadedImage
                   } else if i == 2 {
                       self.predictedImage3 = downloadedImage
                   }
                   i += 1
               }
               
              // print(String(imageArray[0]))
               
           }
         }
        }
        
    }
    
    func downloadImage(image: [String]){
        var downloadedImage: UIImage = UIImage()
        self.imageArray = []
        var i = 0
        for item in image{
            
        
        let storageRef = Storage.storage().reference().child("searchedPlants/\(item).jpeg")
        
        storageRef.getData(maxSize: 6 * 1024 * 1024) { data, error in
           if let error = error {
             // Uh-oh, an error occurred!
               print(error.localizedDescription)
           } else {
               print("Image downloading")
               print(item)
               DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                   downloadedImage = UIImage(data: data!) ?? UIImage()
                   self.imageArray.append(downloadedImage)
                   if i == 0 {
                       self.image = downloadedImage
                   }
                   if i == 1 {
                       self.image2 = downloadedImage
                   } else if i == 2 {
                       self.image3 = downloadedImage
                   }
                   i += 1
               }
               
              // print(String(imageArray[0]))
               
           }
         }
        }
        
    }
    func getPredictedImageNames(){
        self.actualpredictedArray = []
        self.probabilityArray = []
      
        
        let db = Firestore.firestore()
        
        
        db.collection("predictedPlantNames").order(by: "timestamp", descending: true).limit(to: 3).getDocuments(completion: {
            (documents, error) in
            guard let documents = documents else {
                       print("NO DOCUMENTS")
                       return
                   }
           // var i = 0
            for documents in documents.documents{
                let name = documents.get("name") as! String
                let probability = documents.get("probability") as! String
                actualpredictedArray.append(name)
               probabilityArray.append(probability)
            }
          // print(imageNameArray)
            print(actualpredictedArray)
            self.predictedDefaultArray = actualpredictedArray as! [String]
            self.defaultProbArray =  probabilityArray as! [String]
            print(actualpredictedArray)
            print("--------")
            print(predictedDefaultArray)
           
            
        })
    }
    
    func getImageNames(){
        self.imageNameArray = []
        
        let db = Firestore.firestore()
        
        
        db.collection("searchedPlantNames").order(by: "timestamp", descending: true).limit(to: 3).getDocuments(completion: {
            (documents, error) in
            guard let documents = documents else {
                       print("NO DOCUMENTS")
                       return
                   }
            var i = 0
            for documents in documents.documents{
                let name = documents.get("name") as! String
                imageNameArray.append(name)
            }
          // print(imageNameArray)
            print(stringArray)
            self.stringArray = imageNameArray as! [String]
            print(imageNameArray)
            print("--------")
            print(stringArray)
           
            
        })
    }
    
  
    var body: some View {
        
        
        ScrollView(.vertical, showsIndicators: false){
            VStack(alignment: .center, spacing: 20){
                
                // MARK: - HEADER
                
                ScrollView(.horizontal, showsIndicators: false){
                    Header()
                }
                
                
                // MAARK: - PLANTS
                Text("Searched Plants")
                    .font(.system(size: 20, weight: .light, design: .default))
                    .foregroundColor(Color("ColorGreenMedium"))
                   
                ScrollView(.horizontal, showsIndicators: false){
                    HStack(alignment: .top, spacing: 60) {
                        ZStack{
                            
                            Text(stringArray[0].capitalized)
                                .padding(.leading, 90)
                                .padding(.trailing, 10)
                                .padding(.vertical, 3)
                                .frame(width: 250, height: 120, alignment: .center)
                                .background(LinearGradient(gradient: Gradient(colors: [Color("Gradient2"), Color("Gradient22")]), startPoint: .leading, endPoint: .trailing))
                                .cornerRadius(12)
                            .lineLimit(6)
                               // .font(.footnote)
                                .multilineTextAlignment(.leading)
                                .foregroundColor(Color.white)
                            
                            Image(uiImage: image)
                                .resizable()
                                .frame(width: 66, height: 66, alignment: .center)
                                .background(
                            Rectangle()
                                .fill(Color.gray)
                                .frame(width: 74, height: 74, alignment: .center))
                                .cornerRadius(16.0)
                                .offset(x: -80)
                            
                          /*
                           
                           Text("title")
                                 .font(.system(size: 20))
                                 .foregroundColor(.black)
                                 .frame(width: 100, height: 20, alignment: .top)
                                 .lineLimit(1)
                                 .padding(.bottom, 115)*/
                            
                            
                        }
                        ZStack{
                            
                            Text(stringArray[1].capitalized)
                                .padding(.leading, 90)
                                .padding(.trailing, 10)
                                .padding(.vertical, 3)
                                .frame(width: 250, height: 120, alignment: .center)
                                .background(LinearGradient(gradient: Gradient(colors: [Color("Gradient2"), Color("Gradient22")]), startPoint: .leading, endPoint: .trailing))
                                .cornerRadius(12)
                            .lineLimit(6)
                                //.font(.footnote)
                                .multilineTextAlignment(.leading)
                                .foregroundColor(Color.white)
                            
                            Image(uiImage: image2)
                                .resizable()
                                .frame(width: 66, height: 66, alignment: .center)
                                .background(
                            Rectangle()
                                .fill(Color.gray)
                                .frame(width: 74, height: 74, alignment: .center))
                                .cornerRadius(16.0)
                                .offset(x: -80)
                            
                          /*
                           
                           Text("title")
                                 .font(.system(size: 20))
                                 .foregroundColor(.black)
                                 .frame(width: 100, height: 20, alignment: .top)
                                 .lineLimit(1)
                                 .padding(.bottom, 115)*/
                            
                            
                        }
                        ZStack{
                            
                            Text(stringArray[2].capitalized)
                                .padding(.leading, 90)
                                .padding(.trailing, 10)
                                .padding(.vertical, 3)
                                .frame(width: 250, height: 120, alignment: .center)
                                .background(LinearGradient(gradient: Gradient(colors: [Color("Gradient2"), Color("Gradient22")]), startPoint: .leading, endPoint: .trailing))
                                .cornerRadius(12)
                            .lineLimit(6)
                                //.font(.footnote)
                                .multilineTextAlignment(.leading)
                                .foregroundColor(Color.white)
                            
                            Image(uiImage: image3)
                                .resizable()
                                .frame(width: 66, height: 66, alignment: .center)
                                .background(
                            Rectangle()
                                .fill(Color.gray)
                                .frame(width: 74, height: 74, alignment: .center))
                                .cornerRadius(16.0)
                                .offset(x: -80)
                            
                          /*
                           
                           Text("title")
                                 .font(.system(size: 20))
                                 .foregroundColor(.black)
                                 .frame(width: 100, height: 20, alignment: .top)
                                 .lineLimit(1)
                                 .padding(.bottom, 115)*/
                            
                            
                        }
                    }
                    .padding(.vertical)
                    .padding(.leading, 60)
                    .padding(.trailing, 20)
                
                }
                
                Text("Predicted Plants")
                    .font(.system(size: 20, weight: .light, design: .default))
                    .foregroundColor(Color("ColorGreenDark"))
                        
                   
                ScrollView(.horizontal, showsIndicators: false){
                    HStack(alignment: .top, spacing: 60) {
                        ZStack{
                            
                            Text("\(predictedDefaultArray[0]) Probability: %\(defaultProbArray[0])" as String)
                                .padding(.leading, 90)
                                .padding(.trailing, 10)
                                .padding(.vertical, 3)
                                .frame(width: 250, height: 120, alignment: .center)
                                .background(LinearGradient(gradient: Gradient(colors: [Color("Gradient2"), Color("Gradient22")]), startPoint: .leading, endPoint: .trailing))
                                .cornerRadius(12)
                            .lineLimit(6)
                            .font(.system(size: 15))
                            .lineSpacing(10)
                                .multilineTextAlignment(.leading)
                                .foregroundColor(Color.white)
                            
                            Image(uiImage: predictedImage)
                                .resizable()
                                .frame(width: 66, height: 66, alignment: .center)
                                .background(
                            Rectangle()
                                .fill(Color.gray)
                                .frame(width: 74, height: 74, alignment: .center))
                                .cornerRadius(16.0)
                                .offset(x: -80)
                            
                          /*
                           
                           Text("title")
                                 .font(.system(size: 20))
                                 .foregroundColor(.black)
                                 .frame(width: 100, height: 20, alignment: .top)
                                 .lineLimit(1)
                                 .padding(.bottom, 115)*/
                            
                            
                        }
                        ZStack{
                            
                            Text("\(predictedDefaultArray[1]) Probability: %\(defaultProbArray[0])" as String)
                                .padding(.leading, 90)
                                .padding(.trailing, 10)
                                .padding(.vertical, 3)
                                .frame(width: 250, height: 120, alignment: .center)
                                .background(LinearGradient(gradient: Gradient(colors: [Color("Gradient2"), Color("Gradient22")]), startPoint: .leading, endPoint: .trailing))
                                .cornerRadius(12)
                            .lineLimit(6)
                                //.font(.footnote)
                                .font(.system(size: 15))
                                .lineSpacing(10)
                                .multilineTextAlignment(.leading)
                                .foregroundColor(Color.white)
                            
                            Image(uiImage: predictedImage2)
                                .resizable()
                                .frame(width: 66, height: 66, alignment: .center)
                                .background(
                            Rectangle()
                                .fill(Color.gray)
                                .frame(width: 74, height: 74, alignment: .center))
                                .cornerRadius(16.0)
                                .offset(x: -80)
                            
                          /*
                           
                           Text("title")
                                 .font(.system(size: 20))
                                 .foregroundColor(.black)
                                 .frame(width: 100, height: 20, alignment: .top)
                                 .lineLimit(1)
                                 .padding(.bottom, 115)*/
                            
                            
                        }
                        ZStack{
                            
                            Text("\(predictedDefaultArray[2]) Probability: % \(defaultProbArray[2])" as String)
                                .padding(.leading, 90)
                                .padding(.trailing, 10)
                                .padding(.vertical, 3)
                                .frame(width: 250, height: 120, alignment: .center)
                                .background(LinearGradient(gradient: Gradient(colors: [Color("Gradient2"), Color("Gradient22")]), startPoint: .leading, endPoint: .trailing))
                                .cornerRadius(12)
                                .lineSpacing(10)
                                .font(.system(size: 15))
                                
                                
                                .multilineTextAlignment(.leading)
                                .foregroundColor(Color.white)
                           
                            
                            Image(uiImage: predictedImage3)
                                .resizable()
                                .frame(width: 66, height: 66, alignment: .center)
                                .background(
                            Rectangle()
                                .fill(Color.gray)
                                .frame(width: 74, height: 74, alignment: .center))
                                .cornerRadius(16.0)
                                .offset(x: -80)
                            
                          
                           /*
                           Text("title")
                                 .font(.system(size: 20))
                                 .foregroundColor(.black)
                                 .frame(width: 100, height: 20, alignment: .top)
                                 .lineLimit(1)
                                 .padding(.bottom, 80)
                            */
                            
                        }
                    }
                    .padding(.vertical)
                    .padding(.leading, 60)
                    .padding(.trailing, 20)
                }
                    
                
                // MARK: - FOOTER
                VStack (alignment: .center, spacing: 20){
                Text("All about plants")
                    .font(.system(.title, design: .serif))
                    .fontWeight(.bold)
                    .foregroundColor(Color.green)
                    .padding(8)
                    
                    Text("Everything you wanted to know about plants!!")
                        .font(.system(.body, design: .serif))
                        .multilineTextAlignment(.center)
                        .foregroundColor(Color.gray)
                }
                .frame(maxWidth: 640)
                .padding()
                .padding(.bottom, 85)
            }
            .navigationBarHidden(self.isNavigationBarHidden)
            //.navigationBarTitle("")
            .navigationBarBackButtonHidden(true)
        }
        .navigationBarHidden(self.isNavigationBarHidden)
       // .navigationBarTitle("")
        .navigationBarBackButtonHidden(true)
        .edgesIgnoringSafeArea(.all)
        .padding(0)
        .onAppear(perform: {
            getImageNames()
            getPredictedImageNames()
            let seconds = 2.5
            DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
                // Put your code which should be executed with a delay here
                downloadImage(image: stringArray)
                self.isNavigationBarHidden = true
               
                    downloadPredictedImage(image: predictedDefaultArray)
                
                    
                
            }
            
        })
        
    
    }
}


struct Main_Previews: PreviewProvider {
    static var previews: some View {
        Main(plants: plantData)
    }
}
