//
//  SearchBar.swift
//  PlantC
//
//  Created by Sabri Eyyubi on 26.04.2022.
//

import SwiftUI

struct SearchBar: View {
    
    @Binding var data: [dataType]
    @State var txt = ""
    @State var isSearching = false
    var body: some View {
        VStack(spacing: 0){
            
            // MARK: Search bar
            HStack(spacing: 12){
                TextField("Search Plants", text: $txt)
                    .padding(.leading, 24)
                
            }
            .padding()
            .background(Color(.systemGray5))
            .cornerRadius(20.0)
            //.padding(.horizontal)
            
            .overlay(
                HStack{
                    Image(systemName: "magnifyingglass")
                    Spacer()
                    
                    Button(action: {self.txt = ""}){
                        Image(systemName: "xmark.circle.fill")
                    }
                }
                    .padding(.horizontal, 10)
                    .foregroundColor(.gray)
            )
            
            
        }
        .padding()
        .frame(maxHeight: .infinity, alignment: .top)
        .background {
            Color(.systemGray3)
                .ignoresSafeArea()
            
            
        }
        if self.txt != "" {
            List(self.data.filter{$0.name.lowercased().contains(self.txt.lowercased())}) {
                i in
                Text(i.name)
            }
           
        }
    }
}

